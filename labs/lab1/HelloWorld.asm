#data section is used to store the variables
.data
	hello: .ascii "Hello World!\n"	#the string in stored in label hello
	
.text 
			
	la $a0, hello			#loads the address associated with the label hello into the register 
	li $v0, 4			#loads the value 4 into the register in order to print the string
	syscall 			#performs the output
	
	li $v0, 10 			#loads the value 10 in order to terminate the program
	syscall 			#terminates the program
