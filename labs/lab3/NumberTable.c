#include<stdio.h>
#include<math.h>

int main() {
	int i = 1;
	int number;
	printf("Please enter an integer between 1 and 50: \n");
	scanf("%d", &number);
	float res1,res2,res3,res4;
	printf("x\t  1/x^3\t    sqrt(x)\t    log_3(x)\t     1.2^x\n");
	for (i =1;i<=number;i++) {
		printf("%d\t",i);
		res1 = (1.0)/(pow(i,3));
		res2 = sqrt(i);
		res3 = log(i)/log(3);
		res4 = pow(1.2,i);
		printf("%f\t%f\t%f\t%f\n",res1,res2,res3,res4);
	}
}