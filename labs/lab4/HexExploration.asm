.data
	hello:		.asciiz		"Hello world!\n"
	number1:	.word		42
	number2:	.half		21
	number3:	.half		1701
	number4:	.byte		73
	number5:	.half		-1701
	number6:	.byte		127
	number7:	.word		65536
	sum:		.word		0 
	difference:	.word		0
	#######################
	#  ADDRESS   #  DATA  #
	#######################
	# 0x10010010 #    42    #
	# 0x10010011 #    42    #
	# 0x10010012 #    42    #
	# 0x10010013 #    42    #
	# 0x10010014 #    21    #
	# 0x10010015 #    21    #
	# 0x10010016 #    1701    #
	# 0x10010017 #    1701    #
	# 0x10010018 #    73    #
	# 0x10010019 #    nothing    #
	# 0x1001001a #    -1701    #
	# 0x1001001b #    -1701    #
	# 0x1001001c #    127    #
	# 0x1001001d #    nothing    #
	# 0x1001001e #    nothing    #
	# 0x1001001f #    nothing    #
	# 0x10010020 #    65536    #
	# 0x10010021 #    65536    #
	# 0x10010022 #    65536    #
	# 0x10010023 #    65536    #
	# 0x10010024 #    sum    #
	# 0x10010025 #    sum    #
	# 0x10010026 #    sum    #
	# 0x10010027 #    sum    #
	#######################


.text
	#loading the numbers according to their corresponding data type into sequential registers. So .word is loaded using lw , .half using lh and .byte using lb
	lw $t1, number1 
	lh $t2, number2
	lh $t3, number3
	lb $t4, number4
	lh $t5, number5
	lb $t6, number6
	lw $t7, number7
	#adding number3 and number5 and storing the result in $t8
	add $t8 , $t3 , $t5
	#saving the sum as a .word in the label sum
	sw $t8, sum
	#subtracting number5 from number3 and storing the result in $t9
	sub $t9, $t3, $t5
	#saving the difference as a .word in the memory space with label difference
	sw $t9, difference
	la $a0, hello
	li $v0, 4
	syscall
	
	li $v0, 10
	syscall
