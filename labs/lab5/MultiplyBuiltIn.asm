.data 
	str1:	.asciiz		"Please enter first number\n"
	str2:	.asciiz		"Please enter second number\n"
	str3:	.asciiz		"Result: \n"
.text
	#implementing syscall to get integer 1 from user using dialog
	la $a0, str1
	li $v0, 51
	syscall
	#storing the integer into $t1
	move $t1, $a0
	#implementing syscall to get integer 2 from user using dialog
	la $a0, str2
	li $v0, 51
	syscall
	#storing the integer into $t2
	move $t2, $a0
	#multiplying
	mult $t1, $t2
	mflo $t3
	
	la $a0, str3
	li $v0, 4
	syscall
	
	move $a0, $t3
	li $v0, 1
	syscall
	
	li $v0, 10
	syscall
	
