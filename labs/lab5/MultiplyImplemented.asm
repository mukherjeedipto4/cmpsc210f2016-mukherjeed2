.data 

	multiplier:	.word		4
	multiplicand:	.word		3
	product:	.word		0
	str:		.asciiz		"Result : \n"

.text 
	la $t0, multiplier
	lw $t1, 0($t0)
	
	la $t0, multiplicand
	lw $t2, 0($t0)
	
	la $t0, product
	lw $t3, 0($t0)
	
	li $t5, 0	#setting counter equal to 0
	li $t6, 32	#max iterations = 32
	
	
loopTop:
	beq $t5, $t6, loopBottom		#if counter == 32	
	andi $t4, $t1, 1			#getting least significand bit from the multiplier
	beq $t4, $zero, multiplierZero		#if LSB = 0
	bne $t4, $zero, multiplierNotZero	#if LSB not equal to 0
	

loopBottom:					#max iterations reached
	la $a0, str
	li $v0, 4
	syscall
	
	move $a0, $t3
	li $v0, 1
	syscall
	
	li $v0, 10
	syscall
	
multiplierNotZero:				#LSB != 0
	add $t3, $t2, $t3
	sll $t2, $t2, 1
	srl $t1, $t1, 1
	addi $t5, $t5, 1
	j loopTop
	
multiplierZero:					#LSB == 0
	sll $t2, $t2, 1
	srl $t1, $t1, 1
	addi $t5, $t5, 1
	j loopTop
