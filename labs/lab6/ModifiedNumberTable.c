
#include<stdio.h>
#include<math.h>

int main() {
	int count = 0;
	// a = 1/x^3 , b = sqrt(x), c = log_3(x), d = 1.2^x
	char order[] = {'a','b','c','d'};
	char userOrder[4];
	char userInput;
	for(count = 0;count<4;count++) {
		printf("Please enter your selection (a,b,c, or d)\n");
		int k = 0;
		for(k = 0;k<4;k++) {
			switch(order[k]) {
				case 'a':
					printf("a. 1/x^3\n");
					break;
				case 'b':
					printf("b. sqrt(x)\n");
					break;
				case 'c':
					printf("c. log_3(x)\n");
					break;
				case 'd': 
					printf("d. 1.2^x\n");
					break;
				default:
					printf("");
					break;
			}
		}
		//getting the user input. A whitespace is added before %c to convert it into a char
		scanf(" %c",&userInput);
		
		userOrder[count] = userInput;
		//if the user enters one of the 4 selections, the corresponding char in order array is identified and set to 0 
		int j = 0;
		for(int j = 0;j<4;j++) {
			if(userInput == order[j]) {
				order[j] = 0;
			}
		}
	}
	int i = 1;
	int number;
	printf("Please enter an integer between 1 and 50: \n");
	scanf("%d", &number);
	float res_a,res_b,res_c,res_d;
	int j = 0;
	printf("x\t");
	for (j = 0;j<4;j++) {
		switch(userOrder[j]) {
			case 'a':
				printf("  1/x^3\t");
				break;
			case 'b':
				printf("    sqrt(x)\t");
				break;
			case 'c':
				printf("    log_3(x)\t");
				break;
			case 'd': 
				printf("     1.2^x\t");
				break;
		}
	}
	printf("\n");
	
	for (i =1;i<=number;i++) {
		printf("%d\t",i);
		res_a = (1.0)/(pow(i,3));
		res_b = sqrt(i);
		res_c = log(i)/log(3);
		res_d = pow(1.2,i);
		int k = 0;
		for(k = 0;k<4;k++) {
			switch(userOrder[k]) {
				case 'a':
				printf("%f\t",res_a);
				break;
			case 'b':
				printf("%f\t",res_b);
				break;
			case 'c':
				printf("%f\t",res_c);
				break;
			case 'd': 
				printf("%f\t",res_d);
				break;
			}
		}
		printf("\n");
	}
}
