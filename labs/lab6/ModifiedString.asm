.data
	str:		.asciiz		"MIPS_is_awesome!"
	strSpace:	.ascii		" "
	newLine:	.ascii		"\n"
.text

	la $t0, str			# $t0 holds the string address
	
	# PUT A BACKUP OF THE STARTING ADDRESS OF THE STRING INTO $T3
	la $t3, str
	li $t1, 0
	
   loopTop:				# top of our main loop
   	lb $t2, 0($t0)			# load the character at address $t0
   	
   	bne $t2, $zero, notEqual	# jump to notEqual if things aren't equal
   	
   	# At this point, the length of our string (16) is stored in
   	# $t1, and $t0 is currently pointing off the end of the string
   	# in memory at address 0x10010010.
   	
   	# STEP BACK ONE CHARACTER IN THE STRING TO GET
   	# BACK TO 0x1001000f
   	
   	
	addi $t0, $t0 , -1
	#getting the space character into $t5
	la $t4, strSpace
	lb $t5, 0($t4)
	#getting the newLine character into $t6
	la $t4, newLine
	lb $t6, 0($t4)
	#saving the address to the string
	la $t7 , str
	#printing the unmodified string
	move $a0 ,$t7
   	li $v0, 4
   	syscall
   	#printing a newline character
   	move $a0, $t6
  	li $v0, 11
  	syscall
  changingStringTop:
  	#the loop should only go until $t3 <= $t0. Since MIPS cannot directly do that , i used slt. $t1 = 0 if 
  	# $t3 < $t0 and $t1 = 1 if $t3 > $t0
  	slt $t1, $t0, $t3
  	beq $t3, $t0 , loopEnd
  	beq $t1, 1, loopEnd
  	beq $t1, $zero, changingString
  loopEnd:
  	li $v0, 10
  	syscall
	
  changingString:
  	#modifiying the character at the first address
  	sb $t5 , 0($t3)
  	#printing the stirng
  	move $a0, $t7
  	li $v0 , 4
  	syscall
  	
  	move $a0, $t6
  	li $v0, 11
  	syscall
  	#modifying the last character
  	sb $t5, 0($t0)
  	#printting the stirng
  	move $a0, $t7
  	li $v0,4
  	syscall
  	#printing newline
  	move $a0, $t6
  	li $v0, 11
  	syscall
  	#incrementing addresss pointer $t3 and decrementing address pointer $t0
  	addi $t3 , $t3, 1
  	addi $t0, $t0 , -1
  	
  	j changingStringTop
  	
  
   	
  notEqual:
  	addi $t1, $t1, 1		# increment $t1
  	addi $t0, $t0, 1		# move to the next char
  	j loopTop			# jump to the top of the loop
  
  
  	