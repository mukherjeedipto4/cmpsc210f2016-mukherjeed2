#include<stdio.h>
#include<math.h>

//function to convert an int(decimal) to its hex
char getHex(int a) {
	char val;
	switch(a) {
		case 0:
			val = '0';
			break;
		case 1:
			val = '1';
			break;
		case 2:
			val = '2';
			break;
		case 3:
			val = '3';
			break;
		case 4:
			val = '4';
			break;
		case 5:
			val = '5';
			break;
		case 6:
			val ='6';
			break;
		case 7:
			val = '7';
			break;
		case 8: 
			val = '8';
			break;
		case 9:
			val = '9';
			break;
		case 10:
			val = 'a';
			break;
		case 11:
			val = 'b';
			break;
		case 12:
			val = 'c';
			break;
		case 13:
			val = 'd';
			break;
		case 14: 
			val = 'e';
			break;
		case 15:
			val = 'f';
			break;
	}
	return val;
}

int main() {
	float number;
	printf("%s","Please enter a floating point number \n :");
	scanf("%f",&number);
	//converting the memory address into an unsigned so it can be manipulated
	unsigned *n = (unsigned *)(&number);
	unsigned num = *n;
	//array to store the binary bits
	int arr[32];
	int i =0;
	for(i=31;i>=0;i--) {
		int bit = num & 1;
		arr[i] = bit;
		num = num >> 1;
	}
	//printing the different components of the binary representation
	printf("In Binary: \n");
	printf("%d ",arr[0]);
	for(i = 1;i<9;i++) {
		printf("%d",arr[i]);
	}
	printf(" ");
	for(i = 9;i<32;i++) {
		printf("%d",arr[i]);
	}
	printf("\n");
	num = *n;
	//array to store the hex version of the float
	char hexArr[8];
	for(int i =7;i>=0;i--) {
		int bit = num & 0xF;
		hexArr[i] = getHex(bit);
		num = num >> 4;
	}
	printf("In Hexadecimal:\n");
	printf("0x");
	for (int i=0;i<8;i++) {
		printf("%c",hexArr[i]);
	};
	printf("\n");
	num = *n;
	//if MSB = 0, it is positive , else negative
	if(arr[0] == 0) {
		printf("Sign : +\n");
	} else  {
		printf("Sign: -\n");
	}
	//bit masking and shifting to get exponent
	int exponent = num & 0x7F800000;
	exponent = exponent >> 23;
	exponent = exponent - 127;
	float significand;
	int sig_exp = -1;
	//using the array to find the significand
	for(i = 9;i<32;i++) {
		significand = significand + arr[i]*pow(2,sig_exp);
		sig_exp--;
	}
	printf("Exponent: 2^%d\n", exponent);
	//adding the implicit one
	significand = significand + 1;
	printf("significand: %f\n",significand);

	return 0;
}