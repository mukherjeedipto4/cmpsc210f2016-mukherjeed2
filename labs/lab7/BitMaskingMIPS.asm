.data
	number:		.word		0
	significand:	.word		0
	inputString:	.asciiz		"Please enter a floating point number\n"
	newLine:	.asciiz		"\n"
	positive:	.asciiz		"Sign: +\n"
	negative:	.asciiz		"Sign: -\n"
	exponentStr:	.asciiz		"Exponent: 2^"
	significandStr:	.asciiz		"Significand: "
.text
	la $a0, inputString
	li $v0, 52
	syscall
	
	#saving the float as a word and retrievng it so that it can be manipulated easily
	s.s $f0 , number
	la $t0, number 
	lw $t1, 0($t0)				
	
	#hexadecimal representation
	move $a0, $t1
	li $v0, 34					#syscall 34 is hexadecimal print
	syscall 
	
	la $a0 , newLine 
	li $v0, 4
	syscall
	
	#binary
	#we find the MSB in every iteration using bit masking and shift the bits by 31 to get the individual bit. It is then printed accordingly
	move $t6 , $t1
	#printing the sign
	andi $t2, $t1, 0x80000000
	srl $t3, $t2, 31
	
	move $a0, $t3
	li $v0, 1
	syscall
	
	li $a0, ' '
	li $v0, 11
	syscall
	
	sll $t6, $t6, 1
	#printing the exponent
 
	li $t4, 0
	li $t5, 8
 printExponentLoopTop:
	bne $t4, $t5, printExponentLoop
	beq $t4, $t5, printExponentLoopExit
 printExponentLoop:	
	andi $t2, $t6, 0x80000000
	srl $t3, $t2, 31
	
	move $a0, $t3
	li $v0, 1
	syscall
	
	sll $t6, $t6, 1
	addi $t4, $t4, 1
	j printExponentLoopTop
 printExponentLoopExit:
	li $a0, ' '
	li $v0, 11
	syscall
	
	li $t4, 0
	li $t5, 23
 printSignificandLoopTop:
 	bne $t4, $t5, printSignificandLoop
 	beq $t4, $t5, printSignificandLoopExit
 printSignificandLoop:
	andi $t2, $t6, 0x80000000
	srl $t3, $t2, 31
	
	move $a0, $t3
	li $v0, 1
	syscall
	
	sll $t6, $t6, 1
	addi $t4, $t4, 1
	j printSignificandLoopTop
	
 printSignificandLoopExit:
	la $a0 , newLine 
	li $v0, 4
	syscall
	# finding the sign
	andi $t2, $t1, 0x80000000
	srl $t3, $t2, 31
	li $t4, 1
	beq $t3, $zero, positiveSign
	beq $t3, $t4, negativeSign
	
 positiveSign:
	la $a0, positive
	li $v0, 4
	syscall 
	j exponent
	
 negativeSign:
 	la $a0, negative
 	li $v0, 4
 	syscall
 	j exponent
	
	
 exponent:	
	#exponent
	#masking the exponent part and shifting right so that we only get the actual value of the exponent
	andi $t2, $t1, 0x7F800000
	srl $t3, $t2, 23
	addi $t3, $t3, -127
	
	la $a0, exponentStr
	li $v0, 4
	syscall
	
	move $a0, $t3
	li $v0, 1
	syscall
	
	#significand
	la $a0 , newLine 
	li $v0, 4
	syscall
	#masking the significand and adding 01111111 to the exponent part to convert into a float with implicit one
	andi $t2, $t1, 0x007FFFFF
	addi $t2, $t2, 0x3F800000
	
	#storing the significand and loading it as a float so that it is displayed properly
	sw $t2, significand
	
	l.s $f2, significand
	
	la $a0, significandStr
	li $v0, 4
	syscall 
	
	mov.s $f12, $f2
	li $v0, 2
	syscall
	
	#termination
	li $v0, 10
	syscall
	
