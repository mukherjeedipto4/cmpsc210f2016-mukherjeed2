.data
	welcome:	.asciiz		"Welcome to the game computer!\n I am guessing a number between 1 and 100. \n See if you can guess it\n"
	newline:	.asciiz		"\n"
	playAgain:	.asciiz 	"Do you want to play again? (1/0)\n"
	guessHigh:	.asciiz		"Computer, your guess was too high!\n"
	guessLow:	.asciiz		"Computer, your guess was too low\n"
	guessCorrect:	.asciiz		"Computer, your guess was correct\n"
	feedback:	.asciiz		"How did I do?\n"
	computerGuess:	.asciiz		"My guess is "
	guessCorrect1:	.asciiz		"I got it in "
	guessCorrect2:	.asciiz		"tries!"
.text
	mainTop:
	
	jal PROC_WELCOME_MESSAGE
	
	#$t0 = current min value, $t1 = current max value
	li $t0, 1
	li $t1, 100
	#counter
	li $t6, 0
	
 	mainGuess:
	move $a0 , $t0
	move $a1, $t1
	move $a2, $t6
	jal PROC_COMPUTER_GUESS
	move $t6, $v1
	move $t2, $v0
	
	move $a0, $t2
	move $a1, $t0
	move $a2, $t1
	
	jal PROC_USER_CHECK
	move $t3, $v0
	
	beq $t3, 1 , changeUpperLimit
	beq $t3, -1, changeLowerLimit
	beq $t3, 0, displaySuccessMessage
	
	changeUpperLimit:
	move $t1, $t2
	
	j mainGuess
	
	changeLowerLimit:
	move $t0, $t2
	
	j mainGuess
	
	displaySuccessMessage:
	move $a0, $t6
	jal PROC_SUCCESS_MESSAGE
	jal PROC_PLAY_AGAIN
	move $t4, $v0
	
	beq $t4, 1 , mainTop
	beq $t4, 0 ,mainExit
	
	mainExit:
	li $v0, 10
	syscall

	PROC_WELCOME_MESSAGE:
	#prologue
	subi $sp , $sp, 8
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	
	#main body
	la $a0, welcome
	li $v0, 4
	syscall
	
	#epilogue
	lw $ra , 0($sp)
	lw $a0, 4($sp)
	addi $sp, $sp , 8
	#return
	jr $ra
	
	
	PROC_COMPUTER_GUESS:
	#prologue
	subi $sp, $sp, 16
	
	sw $ra , 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $a2, 12($sp)
	#main body
	add $t7, $a0, $a1
	srl $t7, $t7, 1
	
	la $a0, computerGuess
	li $v0, 4
	syscall
	
	move $a0 , $t7
	li $v0 ,1 
	syscall
	
	la $a0, newline
	li $v0, 4
	syscall
	
	addi $a2, $a2, 1
	
	move $v0, $t7
	move $v1, $a2
	
	#epilogue
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $a2, 12($sp)
	addi $sp, $sp, 16
	
	#return 
	jr $ra
	
	PROC_USER_CHECK:
	#prologue
	subi $sp, $sp, 16
	
	sw $ra , 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $a2, 12($sp)
	
	#main body
	la $a0, feedback
	li $v0, 4
	syscall
	
	li $v0, 5
	syscall
	
	move $t7, $v0
	
	beq $v0, -1, guessIsLow
	beq $v0, 1, guessIsHigh
	beq $v0 ,0, guessIsCorrect
	
	guessIsLow:
	la $a0, guessLow
	li $v0, 4
	syscall
	
	j proc_user_check_epilogue
	
	guessIsHigh:
	la $a0, guessHigh
	li $v0, 4
	syscall
	
	j proc_user_check_epilogue
	
	guessIsCorrect:
	la $a0, guessCorrect
	li $v0, 4
	syscall
	
	j proc_user_check_epilogue
	
	#epilogue
	proc_user_check_epilogue:
	move $v0, $t7
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $a2, 12($sp)
	addi $sp, $sp , 16
	
	#return
	jr $ra
	
	PROC_SUCCESS_MESSAGE:
	#prologue
	subi $sp, $sp, 8
	sw $ra , 0 ($sp)
	sw $a0, 4($sp)
	
	#main body
	move $t7, $a0
	
	la $a0, guessCorrect1
	li $v0, 4
	syscall
	
	move $a0, $t7
	li $v0, 1
	syscall
	
	la $a0, guessCorrect2
	li $v0, 4
	syscall
	
	#epilogue
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	addi $sp, $sp, 8
	
	#return 
	jr $ra
	
	PROC_PLAY_AGAIN:
		#prologue
		subi $sp , $sp , 8
		sw $ra, 0($sp)
		sw $a0, 4($sp)
		#main body
		la $a0, playAgain
		li $v0, 4
		syscall
		
		li $v0, 5
		syscall
		
		#epilogue
		lw $ra, 0($sp)
		lw $a0, 4($sp)
		
		#return
		jr $ra
