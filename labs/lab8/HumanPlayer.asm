.data 
	welcome:	.asciiz		"Welcome to the game human!\n I am guessing a number between 1 and 100. \n See if you can guess it\n"
	userInput: 	.asciiz		"Your guess: "
	newline:	.asciiz		"\n"
	playAgain:	.asciiz 	"Do you want to play again? (1/0)\n"
	guessHigh:	.asciiz		"Your guess was too high!\n"
	guessLow:	.asciiz		"Your guess was too low\n"
	guessCorrect:	.asciiz		"Your guess was correct\n"
	guessCorrect1:	.asciiz		"You got it in "
	guessCorrect2:	.asciiz		"tries!"

.text 
	mainTop:
	jal PROC_WELCOME_USER
	#$t0 contains the random number between 1 and 100
	move $t0, $v0
	#counter
	li $t2, 0
	mainInput:
	move $a0, $t2
	jal PROC_USER_INPUT
	#$t1 contains the user input
	move $t1,$v0
	move $t2, $v1
	move $a0, $t1
	move $a1, $t0
	jal PROC_TEST_GUESS
	
	move $t3, $v0
	
	bne $t3, $zero, mainInput
	beq $t3, $zero, correctInputMain
	
	correctInputMain:
	jal PROC_PLAY_AGAIN 
	move $t4, $v0
	
	beq $t4, 1 , mainTop
	beq $t4, 0 ,mainExit
	
	mainExit:
	li $v0, 10
	syscall
	

	PROC_WELCOME_USER:
		#prologue
		subi $sp, $sp, 8
		sw $ra, 0($sp)
		sw $a0, 4($sp)
		
		#main body
		la $a0, welcome
		li $v0, 4
		syscall
		
		
		#getting random number
		li $a0 , 619
		li $a1, 100
		li $v0, 42
		syscall
		
		addi $a0, $a0, 1
		move $v0 , $a0
		
		#epilogue
		lw $a0, 4($sp)
		lw $ra , 0($sp)
		addi $sp, $sp, 8
		
		#return
		jr $ra
	
		
	PROC_USER_INPUT:
		#prologue
		
		subi $sp, $sp, 8
		sw $ra, 0($sp)
		sw $a0, 4($sp)
		
		#main body
		move $t8, $a0
		la $a0 , userInput
		li $v0, 4
		syscall
		
		li $v0 , 5
		syscall
		
		addi $t8, $t8, 1
		move $v1, $t8
		#epilogue
		
		lw $a0 , 4($sp)
		lw $ra , 0($sp)
		addi $sp, $sp, 8
	
		#return
		jr $ra
	
	PROC_TEST_GUESS:
		#prologue
		subi $sp, $sp, 4
		sw $ra , 0($sp)
		
		#main body
		
		blt $a0, $a1, userInputLess
		bgt $a0, $a1, userInputMore
		beq $a0, $a1, userInputSame
	
		userInputLess:
		la $a0, guessLow
		li $v0, 4
		syscall
		
		li $v0 , -1
		
		j proc_test_guess_epilogue
		
		userInputMore:
		la $a0, guessHigh
		li $v0, 4
		syscall
		
		li $v0, 1
		j proc_test_guess_epilogue
		
		userInputSame:
		la $a0, guessCorrect
		li $v0, 4
		syscall
		
		la $a0, guessCorrect1
		li $v0, 4
		syscall
		
		move $a0, $t2
		li $v0, 1
		syscall
		
		la $a0, guessCorrect2
		li $v0, 4
		syscall
		
		li $v0, 0
		j proc_test_guess_epilogue
		
		proc_test_guess_epilogue:
		#epilogue
		lw $ra , 0($sp)
		addi $sp, $sp, 4
		
		#end 
		jr $ra
				
	
	PROC_PLAY_AGAIN:
		#prologue
		subi $sp , $sp , 8
		sw $ra, 0($sp)
		sw $a0, 4($sp)
		#main body
		la $a0, playAgain
		li $v0, 4
		syscall
		
		li $v0, 5
		syscall
		
		#epilogue
		lw $ra, 0($sp)
		lw $a0, 4($sp)
		
		#return
		jr $ra
		

