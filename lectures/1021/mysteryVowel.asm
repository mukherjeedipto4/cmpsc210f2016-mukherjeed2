#Given program finds all the vowels in the string and changes them to a + symbol

.data
	str:		.asciiz 	"Hello CMPSC 210!\n"

.text
	la $t0, str
	li $t1, 'a'
	li $t4, 'e'
	li $t5, 'i'
	li $t6, 'o'
	li $t7, 'u'
	li $t2, '+'
	
   LOOP:
	lbu $t3, 0($t0)			#loads a single bit from the string
  A:					#finds the vowel
	bne $t1, $t3, E
	beq $t1, $t3, SAVE		
  E:
	bne $t4, $t3, I
	beq $t4, $t3, SAVE
  I:
	bne $t5, $t3, O
	beq $t5, $t3, SAVE
 O:
	bne $t6, $t3, U
	beq $t6, $t3, SAVE
 U:
	bne $t7, $t3, NEXT		#checks all vowels. If it does not find anything, it goes  to next
	beq $t7, $t3, SAVE
				
  SAVE:
  	sb $t2, 0($t0)
  	bne $t3, $zero, NEXT
  	beq $t3, $zero, EXIT
		
   NEXT:
   	addi $t0, $t0, 1		#increments the pointer
	bne $t3, $zero, LOOP
	
   EXIT:
   	li $v0, 4			#syscall to print the modified string
   	la $a0, str
   	syscall
   
  	li $v0, 10			#termination syscall
  	syscall
