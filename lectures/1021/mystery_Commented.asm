#Given program finds all the spaces in the string and changes them to a + symbol

.data
	str:		.asciiz 	"Hello CMPSC 210!\n"

.text
	la $t0, str
	li $t1, ' '
	li $t2, '+'
	
   LOOP:
	lbu $t3, 0($t0)			#loads a single bit from the memory
	bne $t1, $t3, NEXT		#if curent char is not a space
	sb $t2, 0($t0)			#if current char is a space, changes it to a + symbol
		
   NEXT:
   	addi $t0, $t0, 1		#incrementing the pointer
	bne $t3, $zero, LOOP		#until it reaches the end
	
   EXIT:
   	li $v0, 4			#print the modified string
   	la $a0, str
   	syscall
   
  	li $v0, 10			#termination syscall
  	syscall
