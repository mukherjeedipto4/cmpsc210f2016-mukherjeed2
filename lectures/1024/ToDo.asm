.data
	list: 	.word 	2, 3, 5, 7, 11, 13,17, 19, 23, 29
	size: 	.word 	10
	
.text 
	lw $s0 , size
	la $s1, list
	li $t0, 0
	
  loop:
  	beq $t0, $s0, exit
  	lw $t1, 0($s1)
  	
  	move $a0, $t1
  	li $v0, 1
  	syscall
  	
  	li $t2, ' '
  	move $a0, $t2
  	li $v0, 11
  	syscall
  	
  	addi $s1, $s1, 4
  	addi $t0, $t0, 1
  	j loop
  	
  exit:
  	li $v0, 10
  	syscall
  	
  